import React, {Component} from 'react'
class Message extends Component{
    constructor(){
        super()
        this.state = {
            message : 'Good Morning'
        }
    }
    render(){
        return(
            <div>
                 <h1> Message is {this.state.message}</h1> 
            </div>
        );
    }
}
export default Message